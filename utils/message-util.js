function messageChecker(text) {
  const hiHelloSubstring = ["hi", "hello"];
  const byeGoodByeSubstring = ["bye", "goodbye"];
  var message = "Sorry, I dont understand."

  if (hiHelloSubstring.some(v => text.toLowerCase().includes(v))) {
    message = "Welcome to StationFive."
  } else if (byeGoodByeSubstring.some(v => text.includes(v))) {
    message = "Thank you, see you around."
  }

  return message;
}

const messageUtils = {
  messageChecker
}

export default messageUtils
