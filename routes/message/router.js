import messageRouter from "./message.js"

function MessageRoute(app) {
  messageRouter(app)
}

export default MessageRoute
