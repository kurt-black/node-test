import messageValidator from '../../middlewares/validators/message.js'
import messageUtils from '../../utils/message-util.js'

function messageRouter(route) {
  route.post(
    '/message',
    messageValidator.messageValidator,
    async (req, res) => {
      const { body } = req

      try {
        const message = await messageUtils.messageChecker(body.message)
        return res.json({
          response_id: body.conversation_id,
          message
        })
      } catch (error) {
        return res.json({
          error: {
            code: 400,
            message: 'Something went wrong'
          }
        })
      }
    }
  )
}

export default messageRouter