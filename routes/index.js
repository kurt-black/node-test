import MessageRouter from './message/router.js'

const Routes = (app) => {

  const register = () => {
    MessageRouter(app)
  }

  return {
    register
  }
}

export default Routes
