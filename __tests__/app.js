import { jest } from '@jest/globals'
import App from '../app.js'

describe('app.js', () => {

  const TEST_PORT = 9000
  let app

  beforeEach(() => {
    app = App().init()
    app.set('port', TEST_PORT)
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  test('check routes', () => {
    const routes = 
      app
        ._router
        .stack
        .filter(({route}) => route !== undefined)
        .map(({route}) => ({
          path: route.path,
          methods: route.methods
        }))

    const messageRoute = routes.find(({path}) => path === '/message') 

    expect(messageRoute).not.toBeNull()
    expect(messageRoute).not.toBe(undefined)
    expect(messageRoute.methods.post).toBeTruthy()
  })
})
