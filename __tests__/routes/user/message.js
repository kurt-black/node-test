import { jest } from '@jest/globals'
import { faker } from '@faker-js/faker'
import request from 'supertest'
import Express from 'express'
import messageRouter from '../../../routes/message/message.js'
import messageUtils from '../../../utils/message-util.js'

describe('routes/message/message.js', () => {

  const TEST_PORT = 5050
  const app = Express()
  const messageData = {
    conversation_id: 'abc123',
    message: faker.lorem.word()
  }

  function getRequest({data}) {
    return request(app).post('/message').send(data)
  }

  beforeAll(() => {
    app.set('port', TEST_PORT)
    app.use(Express.json())
    app.use(Express.urlencoded({
      extended: true
    }))
    messageRouter(app)
  })

  describe('POST /message', () => {

    afterEach(() => {
      jest.clearAllMocks()
    })

    describe('utils', () => {

      test('Should be able to get message: Welcome to StationFive. response without errors', async () => {
        const { body, statusCode } = await getRequest({
          data: {
            conversation_id: '123abc',
            message: 'Hi Kurt'
          }
        })

        expect(body.response_id).toEqual('123abc')
        expect(body.message).toEqual('Welcome to StationFive.')
        expect(statusCode).toBe(200)
      })

      test('Should be able to get message: Thank you, see you around. response without errors', async () => {
        const { body, statusCode } = await getRequest({
          data: {
            conversation_id: '123abc',
            message: 'bye Kurt'
          }
        })

        expect(body.response_id).toEqual('123abc')
        expect(body.message).toEqual('Thank you, see you around.')
        expect(statusCode).toBe(200)
      })

      test('Should be able to get message: Sorry, I dont understand. response without errors', async () => {
        const { body, statusCode } = await getRequest({
          data: {
            conversation_id: '123abc',
            message: 'Whats up Kurt'
          }
        })

        expect(body.response_id).toEqual('123abc')
        expect(body.message).toEqual('Sorry, I dont understand.')
        expect(statusCode).toBe(200)
      })
    })

    describe('endpoint handler', () => {

      test('Should be able to get message response without errors', async () => {
        // const mockedMessageChecker = jest.spyOn(messageUtils, 'messageChecker')
        // mockedMessageChecker.mockResolvedValue('test')
  
        const { body, statusCode } = await getRequest({
          data: messageData
        })
  
        expect(body.response_id).toEqual(messageData.conversation_id)
        expect(statusCode).toBe(200)
        // expect(mockedMessageChecker).toHaveBeenCalledTimes(1)
      })
      
      test('Should be able to handle server error', async () => {
        const mockedMessageChecker = jest.spyOn(messageUtils, 'messageChecker')
        mockedMessageChecker.mockRejectedValue([])
        const { body } = await getRequest({
          data: messageData
        })

        expect(body.error).toEqual({
          code: 400,
          message: 'Something went wrong'
        })
      })
    })

    describe('validations', () => {

      test('Should return 200 status code if Conversation ID is not sent with error.code = 400 and error.message = Conversation ID is required', async () => {
        // const mockedMessageChecker = jest.spyOn(messageUtils, 'messageChecker')
        // mockedMessageChecker.mockResolvedValue('test')

        const { body } = await getRequest({
          data: {
            message: faker.lorem.word()
          }
        })

        expect(body.data).toBeNull()
        expect(body.error).toEqual({
          code: 400,
          message: 'Conversation ID is required'
        })
      })

      test('Should return 200 status code if Message is not sent with error.code = 400 and error.message = Message is required', async () => {
        // const mockedMessageChecker = jest.spyOn(messageUtils, 'messageChecker')
        // mockedMessageChecker.mockResolvedValue('test')

        const { body } = await getRequest({
          data: {
            conversation_id: 'abc123'
          }
        })

        expect(body.data).toBeNull()
        expect(body.error).toEqual({
          code: 400,
          message: 'Message is required'
        })
      })
    })

  })

})
