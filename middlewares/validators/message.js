import Joi from 'joi'

const createSchema = Joi.object({
  conversation_id: Joi.string().required().error(new Error('Conversation ID is required')),
  message: Joi.string().required().error(new Error('Message is required'))
})

function messageValidator(req, res, next) {
  const { error } = createSchema.validate(req.body, {
    abortEarly: true 
  })

  if (error) {
    return res.json({
      data: null,
      error: {
        code: 400,
        message: error.message
      }
    })
  } else {
    return next()
  }
}

const messageValidators = {
  messageValidator
}

export default messageValidators