import Express from 'express'
import Routes from './routes/index.js'

const App = () => {
  const init = () => {

    const app = Express()

    app.use(Express.json())
    app.use(Express.urlencoded({
      extended: true	
    }))

    Routes(app)
      .register()

    return app
  }

  return {
    init
  }
}

export default App
